# coding=utf-8
import uuid
import os

BASE_URL = 'https://indreamsphuket.bpmonline.com'
file_upload_url = '{0}/0/rest/FileApiService/Upload'.format(BASE_URL)

WWW_STATIC_FOLDER = 'http://www.indreamsphuket.ru/uploads/realty/'

GALLERY_URL = "http://bytecodecrm.ru:8080/"


def img_path(p):
    return os.path.join(u'/home/indreams/www/new.indreamsphuket.ru/web/uploads/realty/', p)


config = {
    'user': 'indreams',
    'password': 'T4DIpIt83s|g',
    'host': '127.0.0.1',
    'database': 'indreams_new',
    'raise_on_warnings': True,
    'buffered': True
}
operation_types = {
    "Select": 0,
    "Insert": 1,
    "Update": 2,
    "Delete": 3,
    "Batch": 4
}
expression_types = {
    "SchemaColumn": 0,
    "Function": 1,
    "Parameter": 2,
    "SubQuery": 3,
    "ArithmeticOperation": 4
}
data_value_types = {
    "Guid": 0,
    "Text": 1,
    "Integer": 4,
    "Float": 5,
    "Money": 6,
    "DateTime": 7,
    "Date": 8,
    "Time": 9,
    "Lookup": 10,
    "Enum": 11,
    "Boolean": 12,
    "Blob": 13,
    "Image": 14,
    "ImageLookup": 16,
    "Color": 18,
    "Mapping": 26
}

credentials = {
    'UserName': 'Anna',
    'UserPassword': '9339'
}
headers = {'Content-Type': 'application/json'}
base_view = '{0}/0/Nui/ViewModule.aspx'.format(BASE_URL)
auth_url = '{0}/ServiceModel/AuthService.svc/Login'.format(BASE_URL)

ops = ['SELECT', 'INSERT', 'UPDATE', 'DELETE']
endpoints = {k: '{0}/0/DataService/json/reply/{1}Query'.format(BASE_URL, k.capitalize()) for k in ops}


def generate_skeleton(_uuid, title, price, daily_price, property_type, listing_type):
    return {
        "rootSchemaName": "Listing",
        "operationType": operation_types['Insert'],
        "columnValues": {
            "items": {
                "Id": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Guid'],
                        "value": _uuid
                    }
                },
                "CreatedBy": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "410006e1-ca4e-4502-a9ec-e54d922d2c00"
                    }
                },
                "ModifiedBy": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "410006e1-ca4e-4502-a9ec-e54d922d2c00"
                    }
                },
                "Status": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "4afcfbc3-7629-40c7-b5ac-56d48c0291d2"
                    }
                },
                "Owner": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "410006e1-ca4e-4502-a9ec-e54d922d2c00"
                    }
                },
                "Price": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Integer'],
                        "value": int(price)
                    }
                },
                "BCPricePerDay": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Float'],
                        "value": float(daily_price)
                    }
                },
                "LeadType": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": listing_type
                    }
                },
                "Currency": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "4b64065d-3a20-47e2-b9e4-b9d96ad3c7d4"
                    }
                },
                "Name": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Text'],
                        "value": title
                    }
                },
                "Description": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Text'],
                        "value": title
                    }
                },
                "PropertyCategory": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "fae94ea8-4d9b-4e70-aee2-c0b7bc8d6a5d"
                    }
                },
                "PropertyType": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": property_type
                    }
                },
                "Country": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "b7112b30-7879-4da6-9202-5e2dac7258b3"
                    }
                },
                "City": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Lookup'],
                        "value": "692a9af1-e726-478d-af17-a780b8821ba3"
                    }
                },
                "Address": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['Text'],
                        "value": ""
                    }
                }

            }
        }
    }


def append_photo_id(_uuid, photo_uuid):
    return {
        "rootSchemaName": "Listing",
        "operationType": operation_types['Update'],
        "columnValues": {
            "items": {
                "Photo": {
                    "expressionType": expression_types['Parameter'],
                    "parameter": {
                        "dataValueType": data_value_types['ImageLookup'],
                        "value": photo_uuid
                    }
                }
            }
        },
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {
                        "expressionType": 1,
                        "functionType": 1,
                        "macrosType": 34
                    },
                    "rightExpression": {
                        "expressionType": 2,
                        "parameter": {
                            "dataValueType": 0,
                            "value": _uuid
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }


def erase_photo_id(listing_uuid):
    return append_photo_id(listing_uuid, None)


def select_all(entity):
    return {
        "RootSchemaName": entity,
        "OperationType": operation_types['Select'],
        "AllColumns": True,
        "Columns": {
            "Items": {
            }
        }
    }


def select_img_names():
    return {
        "RootSchemaName": 'ListingGalleryImage',
        "OperationType": operation_types['Select'],
        "AllColumns": False,
        "Columns": {
            "Items": {
                "Name": {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                         "expression": {"expressionType": 0, "columnPath": "Name"}
                         },
                "Listing": {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                            "expression": {"expressionType": 0, "columnPath": "Listing"}
                            }
            }
        }
    }


def select_by_column(entity, col_name, col_value, columns=['Id']):
    obj = {
        "RootSchemaName": entity,
        "OperationType": operation_types['Select'],
        "AllColumns": False,
        "Columns": {
            "Items": {}
        },
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {"expressionType": 0, "columnPath": col_name},
                    "rightExpression": {
                        "expressionType": expression_types['Parameter'],
                        "parameter": {
                            "dataValueType": 0,
                            "value": col_value
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }
    for col in columns:
        obj['Columns']['Items'][col] = {"caption": "", "orderDirection": 0, "orderPosition": -1, "isVisible": True,
                                        "expression": {"expressionType": 0, "columnPath": col}
                                        }
    return obj


def delete_by_column(entity, col_name, col_value):
    return {
        "RootSchemaName": entity,
        "OperationType": operation_types['Delete'],
        "filters": {
            "items": {
                "primaryColumnFilter": {
                    "filterType": 1,
                    "comparisonType": 3,
                    "isEnabled": True,
                    "trimDateTimeParameterToDate": False,
                    "leftExpression": {"expressionType": 0, "columnPath": col_name},
                    "rightExpression": {
                        "expressionType": expression_types['Parameter'],
                        "parameter": {
                            "dataValueType": 0,
                            "value": col_value
                        }
                    }
                }
            },
            "logicalOperation": 0,
            "isEnabled": True,
            "filterType": 6
        }
    }
