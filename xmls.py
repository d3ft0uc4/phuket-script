# coding=utf-8
from constants import *
import csv
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import datetime
import logging
import httplib as http_client
from xml.dom import minidom
import json
import requests
from lxml import html
import codecs
from itertools import groupby
import struct, imghdr, re
from mail_sender import send_mail
import traceback
import datetime
import mysql.connector

logging.basicConfig(filename='{0}-xml.log'.format(str(datetime.date.today())), level=logging.DEBUG)
log = logging.getLogger(__name__)
http_client.HTTPConnection.debuglevel = 1

# XML_BASE_PATH = ""

XML_BASE_PATH = u"/home/indreams/www/new.indreamsphuket.ru/web/uploads/realty/"

ADMIN_MAIL = 'info@indreamsphuket.ru'  # 'valter@bytecodecrm.ru'
cnx = mysql.connector.connect(**config)
csrf = None


def SELECT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['SELECT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            SELECT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def INSERT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['INSERT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            INSERT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def UPDATE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['UPDATE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            UPDATE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def DELETE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['DELETE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            DELETE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def get_rate(currency1='THB', currency2='RUB'):
    try:
        pair = '{0}_{1}'.format(currency1, currency2)
        url = 'http://free.currencyconverterapi.com/api/v5/convert?q={0}&compact=y'.format(pair)
        rate = json.loads(requests.get(url).text)[pair]['val']
        return float(rate)
    except Exception, err:
        print('get rates: ' + str(err))
        rate = 1.0
    return (rate)


def prettify(elem):
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def get_image_size(fname):
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(32)
        if len(head) != 32:
            return 0, 0
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return 0, 0
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0)  # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception:  # IGNORE:W0703
                return 0, 0
        elif imghdr.what(fname) == 'pgm':
            header, width, height, maxval = re.search(
                b"(^P5\s(?:\s*#.*[\r\n])*"
                b"(\d+)\s(?:\s*#.*[\r\n])*"
                b"(\d+)\s(?:\s*#.*[\r\n])*"
                b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", head).groups()
            width = int(width)
            height = int(height)
        elif imghdr.what(fname) == 'bmp':
            _, width, height, depth = re.search(
                b"((\d+)\sx\s"
                b"(\d+)\sx\s"
                b"(\d+))", str).groups()
            width = int(width)
            height = int(height)
        else:
            return 0, 0
        return width, height


# def get_img_dict(img_rows):
#     img_dict = {}
#     for k, g in groupby(img_rows, lambda item: item['Listing']['value']):
#         img_dict[k] = list((map(lambda it: it['Name'], g)))
#     return img_dict

# def get_img_dict(img_rows):
#     img_dict = {}
#     for row in img_rows:
#         listing_id = row['Listing']['value']
#         imgs = img_dict.get(listing_id, [])
#         img = row['Name']
#         imgs.append(img)
#         img_dict[listing_id] = imgs
#     return img_dict


def get_img_dict():
    img_dict = {}
    q = ("SELECT realty_id,filename,sort_order from realty_image")
    cursor = cnx.cursor()
    cursor.execute(q)
    for realty_id, filename, sort_order in cursor:
        imgs = img_dict.get(str(realty_id), [])
        imgs.append((filename, int(sort_order)))
        img_dict[str(realty_id)] = imgs
    for k in img_dict.keys():
        imgs = img_dict[k]
        imgs.sort(key=lambda tup: tup[1])
        imgs = map(lambda tup: tup[0], imgs)
        img_dict[k] = imgs
    return img_dict


def is_img_valid(img_fname):
    w, h = get_image_size(img_fname)
    sizeMB = os.path.getsize(img_fname) >> 20
    if 6000 >= w >= 600 and 6000 >= h >= 600 and sizeMB < 3:
        return True
    else:
        return False


def get_xml(rows, img_dict, placeground):
    incorrect_objects = []
    root = Element('root')
    objects = Element('objects')
    rate = get_rate()
    for row in rows:
        if row[placeground]:
            obj = Element('object')

            # BCSiteID
            id = SubElement(obj, 'id')
            id.text = row['BCSTRINGObjectID']

            language_id = SubElement(obj, 'language_id')
            language_id.text = '1'
            #
            PropertyTypes = {
                '1cb985bb-4eaf-429b-8cd3-3d36e26df32c': '25',
                '28b51a85-033a-41da-945b-2a16ac2f79c6': '2',
                '911f7475-81e9-4b8a-b322-5b6ba30e03c4': '27'
            }
            object_id = SubElement(obj, 'object_id')
            if not isinstance(row['PropertyType'], basestring):
                object_id.text = PropertyTypes.get(row['PropertyType']['value'], '2')
            LeadTypes = {
                '08ca044e-5cf5-4c5f-a091-f6aacd5b53f6': '1'
            }
            if row['BCSTRINGRent']:
                building_type = SubElement(obj, 'building_type')
                if row['BCBOOLEAN']:
                    building_type.text = '117'
                else:
                    building_type.text = '118'
                building_date = SubElement(obj, 'building_date')
                building_date.text = row['BCSTRINGRent']
            if row['Longitude']:
                longitude = SubElement(obj, 'longitude')
                longitude.text = row['Longitude']
            if row['Latitude']:
                latitude = SubElement(obj, 'latitude')
                latitude.text = row['Latitude']
            type_id = SubElement(obj, 'type_id')
            if not isinstance(row['LeadType'], basestring):
                type_id.text = LeadTypes.get(row['LeadType']['value'], '2')
            else:
                type_id.text = '2'
            country_id = SubElement(obj, 'country_id')
            country_id.text = '488'

            region_id = SubElement(obj, 'region_id')
            region_id.text = '57'

            district_id = SubElement(obj, 'district_id')
            district_id.text = '2650'

            address = SubElement(obj, 'address')
            try:
                address.text = row['District']['displayValue']
            except TypeError:
                address.text = ""

            currency_id = SubElement(obj, 'currency_id')
            currency_id.text = '3'
            if type_id.text == '1':
                price = SubElement(obj, 'price')
                price.text = str(float(row['Price']) * rate)
            else:
                price_day = SubElement(obj, 'price_day')
                price_day.text = str(float(row['BCPricePerDay']) * rate)
                price_month = SubElement(obj, 'price_month')
                price_month.text = str(float(row['Price']) * rate)

            # TODO add rooms field
            rooms = SubElement(obj, 'rooms')
            rooms.text = row['BCSTRINGBedroom']

            bedrooms = SubElement(obj, 'bedrooms')
            bedrooms.text = row['BCSTRINGBedroom']

            bathrooms = SubElement(obj, 'bathrooms')
            bathrooms.text = str(row['BCINTEGERBathroom'])

            square = SubElement(obj, 'square')
            square.text = str(row['BCFLOATBuiltUpArea'])

            square_land = SubElement(obj, 'square_land')
            square_land.text = str(row['BCFLOATLandArea'])

            total_floor = SubElement(obj, 'total_floor')
            total_floor.text = row['BCSTRINGFloor']

            description = SubElement(obj, 'description')
            description.text = row['Description']  # row['Name']
            prop_list = []
            if not isinstance(row['BCLOOKUPBeachTo'], basestring):
                if row['BCLOOKUPBeachTo']['value'] not in ['c0dc3194-4a5b-4a73-8c34-bd89b33b3cb6',
                                                           '2659573f-2f32-45ad-970a-385272f606ab']:
                    to_the_beach_dict = {
                        'eb408c78-f038-4ce7-bc06-5b06a968efbb': 50,
                        'effe714b-d7df-4cf9-b98f-023ebe10b2e1': 100,
                        '0d5f162c-c13e-47c1-befc-85f461213048': 200,
                        '442c4599-4a98-419a-8c65-e67757dd15a9': 300,
                        '7fa2d706-8fb8-4a50-9e16-1a1322978f7e': 400,
                        '0969d4c7-96c5-44b2-aa05-855c1d12fc65': 600,
                        '6006c62b-5ede-4639-95ef-c7d3784ebb91': 700,
                        '60ffb304-e7b4-4f7b-8800-094c2b1f469c': 800,
                        'b3ccd2dd-9696-4a26-adfd-37c22fb870c0': 900,
                        '3596e8bf-f4c3-4be5-87d2-1ae28a8d0c6c': 1000,
                        '291c55ba-d087-4b54-9c17-af6f9e44bc53': 1200,
                        'f9864a3f-7064-44b1-b03c-db84beb637b9': 1800,
                        '5f4854f3-049d-44ae-9c9f-df44fe4ff2a3': 1600,
                        '91338868-0aab-43bc-b7e7-fc759838b49b': 1400,
                        '68ef221b-cef5-430e-913c-fd9491f15d05': 2000,
                        '9da0f332-9733-4a30-a26d-964caa47f374': 2500,
                        '0b4330d0-cde8-4ed2-9cb5-c1e54fb82569': 3000,
                        "842659ab-c009-44de-b15d-fe2aa67f1b0f": 3500,
                        '335f0a29-ceb7-4392-95af-c04a7e94d8ad': 4000,
                        'f55201fd-45f7-4d6a-bb3d-d93ae3464b1a': 4500,
                        'd18eac72-f13e-4693-8a6f-58cd885f41d0': 5000,
                        '471960d4-ce24-4471-a871-9f16aba0dc7d': 6000,
                        'daacf952-c7a0-4e5a-b6fb-fba96a8e4660': 7000,
                        'b4449733-b09c-40cd-9e60-65365cea06fc': 8000,
                        'c0fee946-ad57-40ab-bfa6-a8c908a53ea4': 9000,
                        '08ef2a66-a4ce-4165-8771-a5c956769365': 10000
                    }
                    to_the_beach = Element('property')
                    to_the_beach_id = SubElement(to_the_beach, 'property_id')
                    to_the_beach_id.text = '12'
                    to_the_beach_enum = SubElement(to_the_beach, 'property_value_enum')
                    to_the_beach_enum.text = '15'
                    to_the_beach_value = SubElement(to_the_beach, 'property_value')
                    to_the_beach_value.text = str(to_the_beach_dict.get(row['BCLOOKUPBeachTo']['value'], ''))
                    to_the_beach_value_unit = SubElement(to_the_beach, 'property_value_unit')
                    to_the_beach_value_unit.text = '147'
                    prop_list.append(to_the_beach)
            if not isinstance(row['BCLOOKUPFurniturs'], basestring):
                fur_dict = {
                    '25f4eaa6-b306-4b0e-b4ee-af7bda82a272': '51'
                }
                fur = Element('property')
                fur_id = SubElement(fur, 'property_id')
                fur_id.text = '50'
                fur_enum = SubElement(fur, 'property_value_enum')
                fur_enum.text = fur_dict.get(row['BCLOOKUPFurniturs']['value'], '52')
                prop_list.append(fur)
            if row['BCBOOLEANFreeWiFi'] or row['BCBOOLEANInternet'] or row['BCBOOLEANUnlimitedInternet'] or row[
                'BCBOOLEANWiFi']:
                internet = Element('property')
                internet_id = SubElement(internet, 'property_id')
                internet_id.text = '69'
                internet_enum = SubElement(internet, 'property_value_enum')
                internet_enum.text = '71'
                prop_list.append(internet)
            else:
                internet = Element('property')
                internet_id = SubElement(internet, 'property_id')
                internet_id.text = '69'
                internet_enum = SubElement(internet, 'property_value_enum')
                internet_enum.text = '70'
                prop_list.append(internet)

            dict_77 = {
                'BCBOOLEANFridge': '78',
                'BCBOOLEANWasher': '79',
                'BCBOOLEANDishwasher': '80',
                'BCBOOLEANcooker': '81',
                'BCBOOLEANTV': '82',
                'BCBOOLEANMicrowave': '83'
            }

            for k in dict_77.keys():
                if row[k]:
                    prop = Element('property')
                    prop_id = SubElement(prop, 'property_id')
                    prop_id.text = '77'
                    prop_enum = SubElement(prop, 'property_value_enum')
                    prop_enum.text = dict_77[k]
                    prop_list.append(prop)

            dict_39 = {
                'BCBOOLEANViewSee': '41',
                'BCBOOLEANViewOnLake': '43',
                'BCBOOLEANViewOnMontain': '45'
            }
            dict_39_isSet = False
            for k in dict_39.keys():
                if row[k] and not dict_39_isSet:
                    prop = Element('property')
                    prop_id = SubElement(prop, 'property_id')
                    prop_id.text = '39'
                    prop_enum = SubElement(prop, 'property_value_enum')
                    prop_enum.text = dict_39[k]
                    prop_list.append(prop)
                    dict_39_isSet = True
            if type_id.text == '1':
                if row['BCBOOLEANInstallments']:
                    prop = Element('property')
                    prop_id = SubElement(prop, 'property_id')
                    prop_id.text = '90'
                    prop_enum = SubElement(prop, 'property_value_enum')
                    prop_enum.text = '92'
                    prop_list.append(prop)
                else:
                    prop = Element('property')
                    prop_id = SubElement(prop, 'property_id')
                    prop_id.text = '90'
                    prop_enum = SubElement(prop, 'property_value_enum')
                    prop_enum.text = '91'
                    prop_list.append(prop)
            # if row['BCBOOLEANMyselfPool'] or row['BCBOOLEANChildrensPool'] or row['BCBOOLEANCommonPool']:
            #     prop = Element('property')
            #     prop_id = SubElement(prop, 'property_id')
            #     prop_id.text = '105'
            #     prop_enum = SubElement(prop, 'property_value_enum')
            #     prop_enum.text = '106'
            #     prop_list.append(prop)
            # if row['BCBOOLEANTropicalGarden']:
            #     prop = Element('property')
            #     prop_id = SubElement(prop, 'property_id')
            #     prop_id.text = '105'
            #     prop_enum = SubElement(prop, 'property_value_enum')
            #     prop_enum.text = '107'
            #     prop_list.append(prop)
            if not isinstance(row['BCLOOKUPPool'], basestring):
                prop = Element('property')
                prop_id = SubElement(prop, 'property_id')
                prop_id.text = '22'
                prop_enum = SubElement(prop, 'property_value_enum')
                prop_enum.text = '24'
                prop_list.append(prop)

            dict_22 = {
                'BCBOOLEANARestaurant': '25',
                'BCBOOLEANSPAArea': '26',
                'BCBOOLEANSecuritiOut': '27',
                'BCBOOLEANConcierge': '28',
                'BCBOOLEANConciergeFree': '28',
                'BCBOOLEANParkingIn': '29',
                'BCBOOLEANElevator': '30',
                'BCBOOLEANCommonPlaygroundOut': '31',
                'BCBOOLEANShops': '32'
            }
            for k in dict_22.keys():
                if row[k]:
                    prop = Element('property')
                    prop_id = SubElement(prop, 'property_id')
                    prop_id.text = '22'
                    prop_enum = SubElement(prop, 'property_value_enum')
                    prop_enum.text = dict_22[k]
                    prop_list.append(prop)

            properties = SubElement(obj, 'properties')
            properties.extend(prop_list)
            imgs_fnames = img_dict.get(str(row['BCSiteID']), [])
            images = SubElement(obj, 'images')
            images_valid_flag = False
            for img_fname in imgs_fnames:
                if is_img_valid(XML_BASE_PATH + img_fname):
                    images_valid_flag = True
                    images.append(get_img_xml(img_fname))
            if images_valid_flag:
                objects.append(obj)
            else:
                incorrect_objects.append(u'{0}'.format(row['BCSTRINGObjectID']))
    root.append(objects)
    if incorrect_objects and datetime.datetime.now().hour == 12:
        send_mail(ADMIN_MAIL, u'\n'.join(incorrect_objects))
    return root


def get_img_xml(fname):
    image = Element('image')
    filename = SubElement(image, 'filename')
    filename.text = WWW_STATIC_FOLDER + fname
    return image


def write_xml(str_xml, path):
    with codecs.open(path, "w", "utf-8") as f:
        f.write(str_xml)


if __name__ == '__main__':
    try:
        session = requests.Session()
        auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers)
        if auth.status_code == 200:
            csrf = session.cookies.get_dict().get('BPMCSRF', None)
            # img_rows = json.loads(SELECT(session, obj=select_img_names()), encoding='windows-1251')['rows']
            # log.debug(img_rows)
            # img_dict = get_img_dict(img_rows)
            img_dict = get_img_dict()
            # log.info(img_dict)
            rows = json.loads(SELECT(session, obj=select_all('Listing')), encoding='windows-1251')['rows']
            placegrounds = ['BCBOOLEANPrianXML', 'BCBOOLEANPartnersXML', 'BCBOOLEANHomesOverseasXML']
            filenames = [XML_BASE_PATH + name for name in ['prian.xml', 'partners.xml', 'homesoverseas.xml']]
            result_xmls = [prettify(get_xml(rows, img_dict, placeground)) for placeground in placegrounds]
            for idx, xml in enumerate(result_xmls):
                write_xml(xml, filenames[idx])
        log.info("Script completed")
    except Exception as e:
        log.error(e)
        log.error(traceback.format_exc())
