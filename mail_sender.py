# coding=utf-8
import smtplib as smtp
from getpass import getpass
from email.mime.text import MIMEText
from email.header import Header


def send_mail(dest_email, email_text):
    email = u'emailtestf@yandex.ru'
    password = u'emailtestf123'
    subject = u'Некорректные изображения в объектах с ID:'
    msg = MIMEText(email_text.encode('utf-8'), _charset='utf-8')
    msg['Subject'] = Header(subject.encode('utf-8'), charset='utf-8')
    msg['From'] = email
    msg['To'] = dest_email
    server = smtp.SMTP_SSL(u'smtp.yandex.com')
    server.set_debuglevel(1)
    server.ehlo(email)
    server.login(email, password)
    server.sendmail(email, dest_email, msg.as_string())
    server.quit()


if __name__ == '__main__':
    pass
    # send_mail('valter@bytecodecrm.ru', '123123123321')
