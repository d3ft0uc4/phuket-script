# coding=utf-8

import requests
import json
import uuid
import logging
import datetime
from constants import *
import mysql.connector
import httplib as http_client
import sys
from helper_classes import *
from static_props import props
import html2text
import time
from unidecode import unidecode
import re
from bs4 import BeautifulSoup, Tag
from PIL import Image
import traceback

logging.basicConfig(filename='{0}.log'.format(str(datetime.date.today())),
                    format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%d-%m-%Y:%H:%M:%S')
log = logging.getLogger(__name__)
cnx = mysql.connector.connect(**config)
log.setLevel(logging.INFO)
log.info("Current time:{0}".format(datetime.datetime.now()))
# http_client.HTTPConnection.debuglevel = 1

TAG_RE = re.compile(r'<[^>]+>')

csrf = None


def remove_tags(text):
    return TAG_RE.sub('', text)


def populate_static(obj, values):
    for v in values:
        pair = props.get(v, ('', ''))
        if len(pair[0]):
            obj.__dict__['columnValues'].__dict__['items'].__dict__[pair[0]] = pair[1]


def get_result_set():
    cursor = cnx.cursor()
    query = (
        "SELECT model_id,name,daily_price,monthly_price,slug,active,content,sku FROM realty_translation INNER JOIN realty ON realty_translation.model_id = realty.id WHERE language_id = 2")

    static_values_query = (
        "SELECT static_value_id from realty_static_values WHERE model_id=%s"
    )

    eav_query = (
        "SELECT property_id,value_integer,value_float,value_string,value_text from realty_eav WHERE model_id=%s"
    )
    cursor.execute(query)
    result_set = []
    dict_105 = {
        511: u'нет',
        512: u'1 года',
        513: u'2 лет',
        514: u'3 лет',
        515: u'5 лет'
    }
    for model_id, name, daily_price, price, slug, active, content, sku in cursor:
        obj = Listing()
        periods = []
        price_list = []
        obj.Id = Guid(str(uuid.uuid4()))
        if int(active):
            obj.Status = Lookup("4AFCFBC3-7629-40C7-B5AC-56D48C0291D2")
        else:
            obj.Status = Lookup("6B5A6BC1-007B-40D7-9E48-D849C2C402A2")

        obj.CreatedBy = Lookup("410006e1-ca4e-4502-a9ec-e54d922d2c00")
        obj.ModifiedBy = Lookup("410006e1-ca4e-4502-a9ec-e54d922d2c00")
        obj.Owner = Lookup("410006e1-ca4e-4502-a9ec-e54d922d2c00")
        obj.Currency = Lookup("4b64065d-3a20-47e2-b9e4-b9d96ad3c7d4")
        obj.PropertyCategory = Lookup("fae94ea8-4d9b-4e70-aee2-c0b7bc8d6a5d")
        obj.City = Lookup("553D4F15-336C-4DD4-A01A-9351CA0D2C14")
        obj.Country = Lookup("B7112B30-7879-4DA6-9202-5E2DAC7258B3")
        obj.Name = Text(name)
        # Default Property type
        obj.PropertyType = Lookup("1cb985bb-4eaf-429b-8cd3-3d36e26df32c")
        # Default LeadType
        obj.LeadType = Lookup("C1FE9879-C9E1-46AD-AB5B-F7958A4D7E31")
        soup = BeautifulSoup(content, 'lxml')
        for tag in soup.find_all('table'):
            tag.replaceWith('')
        obj.Description = Text(soup.get_text(separator='\n'))
        obj.BCSiteID = Text(str(model_id))
        obj.BCSTRINGObjectID = Text(str(sku))
        obj.BCPricePerDay = Float(daily_price)
        obj.Price = Float(float(price))
        obj.BCSTRINGGallery = Text("http://188.93.211.8:5000/{0}".format(model_id))
        obj.ExternalLink = Text('http://www.indreamsphuket.ru/realty/{0}'.format(slug))
        values = []
        cursor2 = cnx.cursor()
        cursor2.execute(static_values_query, (model_id,))
        for value_id in cursor2:
            values.append(int(value_id[0]))
        populate_static(obj, values)
        if any(x in values for x in {511, 512, 513, 514, 515}):
            buf = u''
            for v in set(values).intersection({511, 512, 513, 514, 515}):
                buf += dict_105[v]
                buf += u';'
            obj.BCSTRINGRassrPeriod = Text(buf)
        cursor3 = cnx.cursor()
        cursor3.execute(eav_query, (model_id,))
        for property_id, value_integer, value_float, value_string, value_text in cursor3:
            if int(property_id) == 18:
                pass
                # obj.BCSTRINGObjectID = Text(value_text)
            elif int(property_id) == 123:
                obj.BCSTRINGannualIncome = Text(value_string)
            elif int(property_id) == 19:
                try:
                    obj.BCFLOATBuiltUpArea = Float(value_text)
                except:
                    pass
            elif int(property_id) == 20:
                try:
                    obj.BCFLOATLandArea = Float(value_text)
                except:
                    pass
            elif int(property_id) == 31:
                obj.BCSTRINGRent = Text(value_text)
            elif int(property_id) == 124:
                obj.BCSTRINGamountBills = Text(value_string)
            elif int(property_id) == 118:
                obj.BCBOOLEANextraBed = Boolean(True)
            elif int(property_id) == 132:  # 100USD
                obj.BCBOOLEANCheckInFrom9 = Boolean(True)
            elif int(property_id) == 133:  # 150USD
                obj.BCBOOLEANCheckInFrom6 = Boolean(True)
            elif int(property_id) == 29:  # site contact
                soup = BeautifulSoup(value_text, 'lxml')
                for tag in soup.find_all('table'):
                    tag.replaceWith('')
                obj.BCSiteContact = Text(soup.get_text(separator='\n'))
            elif int(property_id) == 27 and value_text.strip() != '':
                coords = json.loads(value_text)
                obj.Longitude = Text(coords['longitude'])
                obj.Latitude = Text(coords['latitude'])
                #
                obj.BCSTRINGMapLink = Text(
                    'https://yandex.ru/maps/?mode=search&text={0}%20{1}'.format(coords['latitude'],
                                                                              coords['longitude']))
            elif int(property_id) == 38:
                periods.append(value_text)
            # Payment terms
            elif int(property_id) == 113:
                obj.BCSTRINGPaymentTerm1 = Text(value_text)
            elif int(property_id) == 114:
                obj.BCSTRINGPaymentTerm2 = Text(value_text)
            elif int(property_id) == 115:
                obj.BCSTRINGPaymentTerm3 = Text(value_text)
            elif int(property_id) == 116:
                obj.BCSTRINGPaymentTerm4 = Text(value_text)
            elif int(property_id) == 117:
                obj.BCSTRINGPaymentTerm5 = Text(value_text)
            elif int(property_id) == 99:
                obj.BCSTRINGAmorFee = Text(value_text)
            elif int(property_id) == 102:
                obj.BCSTRINGFreeholdFee = Text(value_text)
            elif int(property_id) == 103:
                obj.BCSTRINGPropFee = Text(value_text)
            elif int(property_id) == 126:
                price_list.append(value_text)
            elif int(property_id) == 127:
                obj.BCSTRINGFee1 = Text(value_string)
            elif int(property_id) == 128:
                obj.BCSTRINGSpecialBusinessFee = Text(value_string)
            elif int(property_id) == 129:
                obj.BCSTRINGOtherFee = Text(value_string)
            elif int(property_id) == 138:
                try:
                    a = BeautifulSoup(value_text, 'lxml')
                    obj.BCSTRINGCalendar = Text(
                        a.find('script')['src'].replace('//', 'https://').replace('embed-js', 'embed'))
                except Exception:
                    pass
        result_set.append((obj, periods, price_list))
    return result_set


def remove_non_ascii(text):
    return unidecode(text)


def SELECT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['SELECT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            SELECT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def INSERT(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['INSERT'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            INSERT(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def UPDATE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['UPDATE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            UPDATE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def DELETE(ses, obj):
    data = json.dumps(obj, encoding='windows-1251')
    # log.debug('Sending request:\n{0}'.format(data))
    if csrf:
        headers['BPMCSRF'] = csrf
    response = ses.post(endpoints['DELETE'], data=data, headers=headers, allow_redirects=False)
    if response.status_code == 302:
        _auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
        if _auth.status_code == 200:
            DELETE(ses, obj)
        else:
            log.critical("Error during re-login!...")
    if response.status_code != 200:
        log.warn(response.text)
    return response.text


def append_img(p, listing_uuid, photo_uuid, ses):
    success = True
    guid = str(uuid.uuid4())
    fname = guid + '.' + p.split('.')[-1]
    try:

        img = Image.open(img_path(p))
        img = img.resize((800, 400), Image.ANTIALIAS)
        img.save(fname)
        # pil open -> resize -> rename -> save -> send -> del
        _file = open(fname, 'rb')
        data = {
            'fileId': photo_uuid,
            'parentColumnName': 'Listing',
            'parentColumnValue': listing_uuid,
            'entitySchemaName': 'ListingGalleryImage',
            'ColumnName': 'Data',
            'fileName': p,
            'totalFileLength': os.path.getsize(fname),
            '_files': fname
        }
        files = {
            'files': _file
        }
        h = {}
        if csrf:
            h['BPMCSRF'] = csrf
        response = ses.post(file_upload_url, data=data, files=files, headers=h, allow_redirects=False)
        log.info(response.status_code)
        try:
            os.remove(fname)
        except OSError:
            pass
        if int(response.status_code) / 100 == 3:
            log.info("Attempting re-login...")
            ses.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
            success = False
        return response.text, success
    except Exception as e:
        traceback.print_exc()
        log.error(e)
        return "", success


def try_decode(text):
    return text


if __name__ == '__main__':
    # json_data = open('skip.json').read()
    skip_ids = []  # [186]  # json.loads(json_data)
    only_id = None
    skip_update = False
    skip_photo = True
    if len(sys.argv) == 2:
        if sys.argv[1] == '--skip-update':
            log.info("Skipping update...")
            skip_update = True
        if sys.argv[1] == '--skip-photo':
            log.info("Skipping photos...")
            skip_photo = True
        else:
            only_id = str(sys.argv[1])
            log.info("Execution only for site_id:{0}".format(only_id))
            log.setLevel(logging.DEBUG)
    log.debug('Starting script...')
    session = requests.Session()
    session.get(BASE_URL)
    auth = session.post(url=auth_url, data=json.dumps(credentials), headers=headers, allow_redirects=False)
    log.info(auth.text)
    if auth.status_code == 200:
        log.info('Login OK...')
        csrf = session.cookies.get_dict().get('BPMCSRF', None)
        result_set = get_result_set()
        if only_id:
            result_set = filter(lambda (a, b, c): a.BCSiteID == only_id, result_set)
        for o, periods, price_list in result_set:
            _uuid = o.Id
            site_id = o.BCSiteID
            log.info('Processing object with BCSiteID:{0}'.format(site_id))
            print 'Processing object with BCSiteID:{0}'.format(site_id)
            tryExist = json.loads(SELECT(session, select_by_column('Listing', 'BCSiteID', site_id)))['rows']
            isExists = len(tryExist)
            print bool(isExists)
            if isExists:
                if skip_update:
                    continue
                else:
                    log.info('Performing UPDATE...')
                    log.debug('1 uuid:{0}'.format(_uuid))
                    _uuid = tryExist[0]['Id']
                    log.debug('2 uuid:{0}'.format(_uuid))
                    o = to_update(o, _uuid)
                    req = json.loads(o.toJSON(), encoding='windows-1251')
                    log.debug(UPDATE(session, req))
            else:
                log.info('Performing INSERT...')
                req = json.loads(o.toJSON(), encoding='windows-1251')
                log.debug(INSERT(session, req))

            # DELETE EXISTING PERIODS FIRST
            if isExists:
                log.debug(DELETE(session, delete_by_column('BCPropertyPeriods3', 'BCListing', _uuid)))
                log.debug(DELETE(session, delete_by_column('BCPriceDetail', 'BCListing', _uuid)))

            for period in periods:
                try:
                    d = json.loads(period, encoding='windows-1251')
                    keys = d.keys()
                    if 'name' in keys:
                        keys.remove('name')
                    for k in keys:
                        p = BCPropertyPeriods3()
                        p.Id = Guid(str(uuid.uuid4()))
                        p.BCListing = Lookup(_uuid)
                        p.BCDatesPeriods = Text(d['name'])
                        p.Name = Text(k)
                        p.Description = Text(d[k])
                        log.debug(INSERT(session, json.loads(p.toJSON(), encoding='windows-1251')))
                except:
                    pass

            for price in price_list:
                try:
                    d = json.loads(price, encoding='windows-1251')
                    p = BCPriceDetail()
                    p.Id = Guid(str(uuid.uuid4()))
                    p.BCListing = Lookup(_uuid)
                    row = u' | '.join(map(lambda key: unicode(key) + u' - ' + unicode(d[key]), d))
                    p.BCRow = Text(row)
                    log.debug(INSERT(session, json.loads(p.toJSON(), encoding='windows-1251')))
                except:
                    pass
            if not skip_photo and int(site_id) not in skip_ids:
                model_id = o.BCSiteID
                log.debug('Model_ID: {0}'.format(model_id))
                get_img_fname = ("SELECT filename from realty_image WHERE realty_id=%s ORDER BY sort_order ASC ")
                cursor = cnx.cursor()
                cursor.execute(get_img_fname, (model_id,))
                if isExists:
                    erase_photo_id(_uuid)
                    log.debug(
                        DELETE(session,
                               delete_by_column(entity='ListingGalleryImage', col_name='Listing', col_value=_uuid)))
                ph_uuid = None
                for filename in cursor:
                    photo_uuid = str(uuid.uuid4())
                    if ph_uuid is None:
                        ph_uuid = photo_uuid
                    log.info('Appending filename:{0}'.format(filename))
                    response, success = append_img(filename[0], _uuid, photo_uuid, session)
                    log.debug(response)
                    if not success and ph_uuid == photo_uuid:
                        ph_uuid = None
                cursor.close()
                if ph_uuid:
                    log.debug(UPDATE(session, append_photo_id(_uuid, ph_uuid)))
    log.debug('Script completed..')
